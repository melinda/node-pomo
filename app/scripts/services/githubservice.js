'use strict';

angular.module('pomoApp')
  .factory('githubService', ['$http', function ($http) {
    // Service logic
    // ...

    var doRequest = function(username, path) {
      return $http({
        method: 'JSONP',
        url: 'https://api.github.com/' + username + '/' + path + '?callback=JSON_CALLBACK'
      });
    };
    return {
      events: function(username) { return doRequest(username, 'events'); }
    };
  }]);