'use strict';

angular.module('pomoApp')
    .factory('Issues', ['$resource', function ($resource) {
        // Service logic
        return $resource(
            'issues.json', {}, {
                query: { method: 'GET', isArray: false },
        });

  }]);