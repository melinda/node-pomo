app.factory('timer', ['issue', function(issue) {
  var timer = {
    running: false,
    current: null,
    ready: false,

    start: function(session) {
      // If we are running, stop the current timer
      if (timer.running) timer.stop();
      
      timer.current = session; // Store the current program
      session.start(); // Start timer
      timer.running = true
    },

    stop: function() {
      if (timer.running) {
        session.pause(); // stop playback
        // Clear the state of the player
        timer.ready = timer.running = false; 
        timer.current = null;
      }
    }
  };
  return timer;
}]);