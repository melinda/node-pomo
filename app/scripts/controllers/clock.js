'use strict';

angular.module('pomoApp')
  .controller('ClockCtrl', function ($scope) {

    var updateClock = function() {
      $scope.clock = new Date().getTime();
    };
    var timer = setInterval(function() {
      $scope.$apply(updateClock);
    }, 1000);
    updateClock();

  });