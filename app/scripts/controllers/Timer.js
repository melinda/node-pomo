'use strict';

angular.module('pomoApp')
  .controller('TimerCtrl', function ($scope, $localStorage, $sessionStorage) {
    $scope.running = false;
    $scope.status = "not-running";
    $scope.started = false;

    $scope.sayHello = function (issue) {
      $scope.current = $scope.$storage.currentIssue = issue;
      console.log($scope.$storage.currentIssue);
      issue.sessionsCount++;
    }

    $scope.timer = function(issue, duration) {
      $scope.issue = issue;
      $scope.duration = duration * 1000;
      $scope.started = new Date().getTime();
      $scope.running = true;
      $scope.status = "running";
      var count = 0;

      var updateClock = function() {
        $scope.now = new Date().getTime();
        $scope.elapsed = Math.floor( ($scope.now - $scope.started) / 1000) + 1;
        $scope.remaining = Math.floor( ($scope.duration/1000) - ++count);
        console.log($scope.remaining);
        if ($scope.remaining <= 0) {
          $scope.end();
        }
      };
    
      $scope.start = setInterval(function() {
        $scope.$apply(updateClock);
      }, 1000);
      updateClock();
      }

      $scope.end = function() {
        clearInterval($scope.start);
        $scope.issue.sessionsCount++;
        $scope.running = false;
        $scope.issue.totalTime = $scope.issue.totalTime + $scope.elapsed;
      }
    
      $scope.timer.stop = function() {
        $scope.end();
      }


  });
