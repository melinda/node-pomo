'use strict';

describe('Controller: ServicecontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('pomoApp'));

  var ServicecontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ServicecontrollerCtrl = $controller('ServicecontrollerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
